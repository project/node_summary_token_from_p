Node Summary Token From P Tags
==============================

When a node doesn't contain a body-field, the [node:summary] token will not
output anything. This module sets the [node:summary] token to the first three
sentences found in <p> tags in the rendered node's HTML.
